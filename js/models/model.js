function Employee(
  _account,
  _name,
  _email,
  _password,
  _daywork,
  _salary,
  _potition,
  _hour
) {
  this.account = _account;
  this.name = _name;
  this.email = _email;
  this.password = _password;
  this.daywork = _daywork;
  this.salary = _salary;
  this.potition = _potition;
  this.hour = _hour;
  this.sumSalary = function () {
    var sumSalary = 0;
    if (this.potition == "Sếp") {
      return (sumSalary = this.salary * 3);
    } else if (this.potition == "TP") {
      return (sumSalary = this.salary * 2);
    } else {
      return (sumSalary = this.salary);
    }
  };
  this.rate = function () {
    var rankEmp = "";
    if (this.hour < 160) {
      return (rankEmp = "nhân viên trung bình");
    } else if (this.hour >= 160 && this.hour < 176) {
      return (rankEmp = "nhân viên khá");
    } else if (this.hour >= 160 && this.hour < 192) {
      return (rankEmp = "nhân viên giỏi");
    } else {
      return (rankEmp = "nhân viên xuất sắc");
    }
  };
}
