const EMPLOYEELIST = "EmployeeList";
var employeeList = [];
var dataJson = localStorage.getItem(EMPLOYEELIST);
if (dataJson) {
  var dataRaw = JSON.parse(dataJson);
  var result = [];
  for (var index = 0; index < dataRaw.length; index++) {
    var currentData = dataRaw[index];
    var employee = new Employee(
      currentData.account,
      currentData.name,
      currentData.email,
      currentData.password,
      currentData.daywork,
      currentData.salary,
      currentData.potition,
      currentData.hour
    );
    result.push(employee);
  }
  employeeList = result;
  renderEmployeeList(employeeList);
}
function saveLocalStorage() {
  var empListJson = JSON.stringify(employeeList);
  localStorage.setItem(EMPLOYEELIST, empListJson);
}
//thêm NV
function addEmployee() {
  var newEmp = getData();
  var isValid = true;
  isValid &=
    checkBlank(newEmp.account, "tbTKNV") &&
    checkLength(newEmp.account, "tbTKNV") &&
    checkAcc(newEmp.account, employeeList, "tbTKNV");
  isValid &=
    checkBlank(newEmp.name, "tbTen") && checkName(newEmp.name, "tbTen");
  isValid &=
    checkBlank(newEmp.email, "tbEmail") && checkEmail(newEmp.email, "tbEmail");
  isValid &=
    checkBlank(newEmp.daywork, "tbNgay") && checkDay(newEmp.daywork, "tbNgay");
  isValid &=
    checkBlank(newEmp.salary, "tbLuongCB") &&
    checkSalary(newEmp.salary, "tbLuongCB");
  isValid &=
    checkBlank(newEmp.potition, "tbChucVu") &&
    checkPos(newEmp.potition, "tbChucVu");
  isValid &=
    checkBlank(newEmp.hour, "tbGiolam") && checkHour(newEmp.hour, "tbGiolam");
  isValid &=
    checkBlank(newEmp.password, "tbMatKhau") &&
    checkPass(newEmp.password, "tbMatKhau");
  if (isValid) {
    employeeList.push(newEmp);
    saveLocalStorage();
    renderEmployeeList(employeeList);
    resetForm();
    Swal.fire("Thêm thành công");
  }
}
//xóa NV
function deleteEmployee(accEmp) {
  var index = employeeList.findIndex(function (employee) {
    return employee.account == accEmp;
  });
  if (index == -1) {
    return;
  }
  employeeList.splice(index, 1);
  renderEmployeeList(employeeList);
  saveLocalStorage();
  Swal.fire("Xoá thành công");
}
//sửa NV
function editEmployee(accEmp) {
  var index = employeeList.findIndex(function (employee) {
    return employee.account == accEmp;
  });
  if (index == -1) return;
  var employee = employeeList[index];
  showData(employee);
}
//cập nhật
function updateEmployee() {
  var empEdit = getData();
  var index = employeeList.findIndex(function (employee) {
    return employee.account == empEdit.account;
  });
  if (index == -1) return;
  var isValid = true;
  isValid &=
    checkBlank(empEdit.name, "tbTen") && checkName(empEdit.name, "tbTen");
  isValid &=
    checkBlank(empEdit.email, "tbEmail") &&
    checkEmail(empEdit.email, "tbEmail");
  isValid &=
    checkBlank(empEdit.daywork, "tbNgay") &&
    checkDay(empEdit.daywork, "tbNgay");
  isValid &=
    checkBlank(empEdit.salary, "tbLuongCB") &&
    checkSalary(empEdit.salary, "tbLuongCB");
  isValid &=
    checkBlank(empEdit.potition, "tbChucVu") &&
    checkPos(empEdit.potition, "tbChucVu");
  isValid &=
    checkBlank(empEdit.hour, "tbGiolam") && checkHour(empEdit.hour, "tbGiolam");
  isValid &=
    checkBlank(empEdit.password, "tbMatKhau") &&
    checkPass(empEdit.password, "tbMatKhau");
  if (isValid) {
    employeeList[index] = empEdit;
    saveLocalStorage();
    renderEmployeeList(employeeList);
    resetForm();
    Swal.fire("Sửa thành công");
  }
}
// search NV
function search() {
  var rateEmp = document.getElementById("searchName").value;

  var filterEmp = employeeList.filter(function (employee) {
    return employee.rate() == rateEmp;
  });
  renderEmployeeList(filterEmp);
}
