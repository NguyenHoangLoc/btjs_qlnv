function checkBlank(value, idError) {
  if (value.length == 0) {
    document.getElementById(idError).style.display = "block";
    document.getElementById(idError).innerText =
      "trường này không được để rỗng";
    return false;
  } else {
    document.getElementById(idError).innerText = "";
    return true;
  }
}
function checkLength(value, idError) {
  if (value.length < 7 && value.length > 3) {
    document.getElementById(idError).innerText = "";
    return true;
  } else {
    document.getElementById(idError).style.display = "block";
    document.getElementById(idError).innerText = "trường này không hợp lệ";
    return false;
  }
}
function checkAcc(idEmp, listEmp, idError) {
  var index = listEmp.findIndex(function (employee) {
    return employee.account == idEmp;
  });
  if (index == -1) {
    document.getElementById(idError).innerText = "";
    return true;
  } else {
    document.getElementById(idError).style.display = "block";
    document.getElementById(idError).innerText = "tài khoản đã tồn tại";
    return false;
  }
}
function checkEmail(value, idError) {
  const re =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  var isEmail = re.test(value);
  if (!isEmail) {
    document.getElementById(idError).style.display = "block";
    document.getElementById(idError).innerText =
      " định dạng email không hợp lệ";
    return false;
  } else {
    document.getElementById(idError).innerText = "";
    return true;
  }
}
function checkName(value, idError) {
  const result = /^[a-z\s]+$/i;
  var isName = result.test(value);
  if (isName) {
    document.getElementById(idError).innerText = "";
    return true;
  } else {
    document.getElementById(idError).style.display = "block";
    document.getElementById(idError).innerText = "tên phải là chữ";
    return false;
  }
}
function checkPass(value, idError) {
  var regularExpression = new RegExp(
    "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^A-Za-z0-9_])"
  );
  var result = regularExpression.test(value);
  if (!result) {
    document.getElementById(idError).style.display = "block";
    document.getElementById(idError).innerText =
      "password không đúng định dạng";
    return false;
  } else {
    document.getElementById(idError).innerText = "";
    return true;
  }
}
function checkDay(value, idError) {
  var reGoodDate = new RegExp(
    "^((0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01])[- /.](19|20)?[0-9]{2})*$"
  );
  var isDay = reGoodDate.test(value);
  if (!isDay) {
    document.getElementById(idError).style.display = "block";
    document.getElementById(idError).innerText =
      "ngày tháng năm không đúng định dạng";
    return false;
  } else {
    document.getElementById(idError).innerText = "";
    return true;
  }
}
function checkSalary(value, idError) {
  if (value <= 20000000 && value >= 1000000) {
    document.getElementById(idError).innerText = "";
    return true;
  } else {
    document.getElementById(idError).style.display = "block";
    document.getElementById(idError).innerText =
      "vui lòng nhập lương trong khoảng 1.000.000-20.000.000";
    return false;
  }
}
function checkPos(value, idError) {
  if (value == "Chọn chức vụ") {
    document.getElementById(idError).style.display = "block";
    document.getElementById(idError).innerText = "vui lòng chọn chức vụ";
    return false;
  } else {
    document.getElementById(idError).innerText = "";
    return true;
  }
}
function checkHour(value, idError) {
  if (value <= 200 && value >= 80) {
    document.getElementById(idError).innerText = "";
    return true;
  } else {
    document.getElementById(idError).style.display = "block";
    document.getElementById(idError).innerText =
      "vui lòng nhập giờ làm trong khoảng 80-200";
    return false;
  }
}
