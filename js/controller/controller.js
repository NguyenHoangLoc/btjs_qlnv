function getData() {
  var accountEmp = document.getElementById("tknv").value;
  var nameEmp = document.getElementById("name").value;
  var emailEmp = document.getElementById("email").value;
  var passwordEmp = document.getElementById("password").value;
  var dayworkEmp = document.getElementById("datepicker").value;
  var salaryEmp = document.getElementById("luongCB").value;
  var potitionEmp = document.getElementById("chucvu").value;
  var hourEmp = document.getElementById("gioLam").value;
  var employee = new Employee(
    accountEmp,
    nameEmp,
    emailEmp,
    passwordEmp,
    dayworkEmp,
    salaryEmp,
    potitionEmp,
    hourEmp
  );
  return employee;
}
function renderEmployeeList(list) {
  var contentHTML = "";
  for (var i = 0; i < list.length; i++) {
    var currentEmp = list[i];
    var contentTr = `<tr> 
        <td>${currentEmp.account}</td>
        <td>${currentEmp.name}</td>
        <td>${currentEmp.email}</td>
        <td>${currentEmp.daywork}</td>
        <td>${currentEmp.potition}</td>
        <td>${currentEmp.sumSalary()}</td>
        <td>${currentEmp.rate()}</td>
        <td>
        <button onclick="deleteEmployee('${
          currentEmp.account
        }')"  class="btn btn-danger">Xoá</button>
    
        <button
        data-toggle="modal"
        data-target="#myModal"
        onclick="editEmployee('${currentEmp.account}')"
        class="btn btn-primary">Sửa</button>
        </td>
        </tr>`;
    contentHTML = contentHTML + contentTr;
  }
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}
function showData(employee) {
  document.getElementById("tknv").value = employee.account;
  document.getElementById("name").value = employee.name;
  document.getElementById("email").value = employee.email;
  document.getElementById("password").value = employee.password;
  document.getElementById("datepicker").value = employee.daywork;
  document.getElementById("luongCB").value = employee.salary;
  document.getElementById("chucvu").value = employee.potition;
  document.getElementById("gioLam").value = employee.hour;
}
function resetForm() {
  document.getElementById("formEmployee").reset();
}
